﻿//see http://www.onlineaspect.com/2010/01/15/backwards-compatible-postmessage/
// everything is wrapped in the XD function to reduce namespace collisions
var XD = function () {

    var interval_id,
    last_hash,
    cache_bust = 1,
    attached_callback,
    window = this;

    return {
        postMessage: function (message, target_url, target) {
            if (!target_url) {
                return;
            }
            target = target || parent;  // default to parent
            if (window['postMessage']) {
                // the browser supports window.postMessage, so call it with a targetOrigin
                // set appropriately, based on the target_url parameter.
                target['postMessage'](message, target_url.replace(/([^:]+:\/\/[^\/]+).*/, '$1'));
            } else {
                // the browser does not support window.postMessage, so use the window.location.hash fragment hack
                //Changed to ignore the target_url parameter
                target.location = target.location.replace(/#.*$/, '') + '#' + (+new Date) + (cache_bust++) + '&' + message;
            }
        },
        receiveMessage: function (callback, source_origin) {
            // browser supports window.postMessage
            if (window['postMessage']) {
                // bind the callback to the actual event associated with window.postMessage
                if (callback) {
                    attached_callback = function (e) {
                        if ((typeof source_origin === 'string' && e.origin !== source_origin)
                        || (Object.prototype.toString.call(source_origin) === "[object Function]" && source_origin(e.origin) === !1)) {
                            return !1;
                        }
                        callback(e);
                    };
                }
                if (window['addEventListener']) {
                    window[callback ? 'addEventListener' : 'removeEventListener']('message', attached_callback, !1);
                } else {
                    window[callback ? 'attachEvent' : 'detachEvent']('onmessage', attached_callback);
                }
            } else {
                // a polling loop is started & callback is called whenever the location.hash changes
                interval_id && clearInterval(interval_id);
                interval_id = null;
                if (callback) {
                    interval_id = setInterval(function () {
                        var hash = document.location.hash,
                         re = /^#?\d+&/;
                        if (hash !== last_hash && re.test(hash)) {
                            last_hash = hash;
                            callback({ data: hash.replace(re, '') });
                        }
                    }, 100);
                }
            }
        }
    };
} ();

var CatfishEditorCommunication = function() {
    return {        
        MessageConstants: {
            ItemAddedtoSavedProjects: "ItemSavedProject",
            ItemAddedtoBasket: "ItemAddedToBasket",
            Unkown: "Unkown"
        },
        RegisterForCatfishEditorEvent: function (callback, source_origin) {
            //simple wrapper 
            XD.receiveMessage(function (message) {
                var data = message.data.split("|");
                if(data.length == 2) {
                    switch (data[0]) {
                        case "ItemAddedToBasket":
                            callback(CatfishEditorCommunication.MessageConstants.ItemAddedtoBasket, parseInt(data[1]));
                            break;
                        case "ItemSavedProject":
                            callback(CatfishEditorCommunication.MessageConstants.ItemAddedtoSavedProjects, parseInt(data[1]));
                            break;
                        default:
                            callback(CatfishEditorCommunication.MessageConstants.Unkown,-1);
                            break;
                    }
                }                
            }, source_origin);
        }
    };
} ();