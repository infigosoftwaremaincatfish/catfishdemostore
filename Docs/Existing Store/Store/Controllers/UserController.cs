﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/

        public ActionResult Index()
        {
            return View( MvcApplication.Current.GetCurrentUserId());
        }

        /// <summary>
        /// Set ther userid for new visitors who do not have a session
        /// </summary>
        /// <returns></returns>
        public ActionResult NewUser()
        {
            MvcApplication.Current.SetCurrentUserId(Guid.NewGuid());
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Saves the user identifier for the session.
        /// </summary>
        /// <param name="guid">The unique identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveUserId(Guid guid)
        {
            MvcApplication.Current.SetCurrentUserId(guid);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Migrates the user from an anonymous to registered user and copies the cart items.
        /// </summary>
        /// <param name="guid">Registered users Guid</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult MigrateUser(Guid guid)
        {
            var currentId = MvcApplication.Current.GetCurrentUserId();
            API.CreateClient().User.Migrate(currentId, guid);
            MvcApplication.Current.SetCurrentUserId(guid);

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Get's the logonURL and redirects the user to it to log them into both stores
        /// </summary>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns></returns>
        public ActionResult LogOnOtherSite(string returnUrl = null)
        {
            var userId = MvcApplication.Current.GetCurrentUserId();
            var result = API.CreateClient().User.GetCustomerSingleSignOnUrl(userId, returnUrl);

            var url = "http://" + API.URL + result.LoginUrl;
            return Redirect(url);
        }
    }
}
