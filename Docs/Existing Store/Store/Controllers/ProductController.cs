﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store.Controllers
{
    public class ProductController : Controller
    {

        //
        // GET: /Product/

        public ActionResult Index()
        {
            return View(MvcApplication.Products.ToList());
        }

        //
        // GET: /Product/Edit/5

        public ActionResult Edit(int id = 0)
        {
            var editorUrl = "http://" + API.URL + API.CreateClient().User.GetEditorLinkFor(MvcApplication.Current.GetCurrentUserId(), id);
            ViewBag.EditorUrl = editorUrl;

            return View();
        }
        
        //
        // GET: /Product/Edit/v5

        public ActionResult EditExisting(string lineItemId, int productId)
        {
            //Get the editor link for the iframe contents
            ViewBag.EditorUrl = "http://" + API.URL + API.CreateClient().User.GetEditorLinkFor(MvcApplication.Current.GetCurrentUserId(), productId, lineItemId);

            return View("Edit");
        }

        public ActionResult PerformTest()
        {
            var error = string.Empty;
            try
            {
                var client = API.CreateClient();
                var productId = client.Catalog.CreateProduct(new CatfishApiSharp.Domain.Catalog.CreateProductRequest()
                    {
                        Description = "Example Description",
                        Name = "Example Product From API",
                        Price = 33.22M,
                        Tags = new string[] { "Alf", "Peter", "Doug" }
                    });

                if (productId > 0)
                {
                    try
                    {
                        client.Catalog.UploadPdf(productId, @"C:\temp\Karte.pdf");
                        error = "all fine we created " + productId;

                        client.Catalog.UploadProductImage(productId, @"C:\temp\shadowa.png");
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                    }

                    
                }
            }
            catch(Exception ex)
            {
                error = ex.Message;
            }

            return View("PerformTest", error);
        }
    }
}