﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CatfishApiSharp.Domain.OrderClient;
using Store.Controllers;
using WebGrease.Css.Extensions;

namespace Store
{
    public class CartController : Controller
    {

        //
        // GET: /Cart/

        public ActionResult Index()
        {
            //Load the items in the users Catfish Basket
            var cartItemIds = API.CreateClient().Orderline.GetBasketItemsForUser(MvcApplication.Current.GetCurrentUserId());


            //Load the details for each of the items
            var CartItems = cartItemIds.Select(item => API.CreateClient().Orderline.GetOrderLineItemInfo(item))
                .Select(current => new CartItem()
                    {
                        Name = current.Name,
                        LineItemId = current.Id,
                        ProductId = current.ProductId,
                        Price = current.Price,
                        UserId = MvcApplication.Current.GetCurrentUserId(),
                        Quantity = current.Quantity,
                        Thumbnail = current.ThumbnailUrls != null ? current.ThumbnailUrls.FirstOrDefault() : string.Empty
                    }
                );

            //Hold the userId on the page
            ViewBag.UserId = MvcApplication.Current.GetCurrentUserId();

            return View(CartItems);
        }

        /// <summary>
        /// Deletes the line from the basket.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ActionResult Delete(string id)
        {
            API.CreateClient().Orderline.DeleteOrderLineItem(id);

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Submit the items in the basket as an order.
        /// </summary>
        /// <returns></returns>
        public ActionResult Checkout()
        {
            //Hard coaded address for testing. Needs to be taken from the page form in production
            Address address = new Address()
            {
                AddressLine1 = "My House",
                AddressLine2 = "My Road",
                Country = "Our Country",
                Email = "Some@web.site",
                FirstName = "My Name",
                LastName = "My LastName",
                StateProvince = "My County",
                Telephone = "01818 11 81 81",
                Town = "My Town",
                ZipPostalCode = "My Postcode"
            };

            //Place the order with Catfish
            var newOrderId = API.CreateClient().Order.PlaceOrder(MvcApplication.Current.GetCurrentUserId(), address, address, null, false);

            return RedirectToAction("CheckoutConfirm", new { newOrderId = newOrderId });
        }

        /// <summary>
        /// Updates the quantity in the catfish basket.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="quantity">The new quantity.</param>
        /// <returns></returns>
        public ActionResult UpdateQuantity(string id, int quantity)
        {
            API.CreateClient().Orderline.UpdateOrderLineItemQuantity(id, quantity);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Display the checkout confirmation.
        /// </summary>
        /// <param name="newOrderId">The new order identifier.</param>
        /// <returns></returns>
        public ActionResult CheckoutConfirm(int newOrderId)
        {
            //Load order info from Catfish
            var orderInfo = API.CreateClient().Order.GetOrderInfo(newOrderId);

            return View(orderInfo);
        }

        /// <summary>
        /// Migrates the cart from the anonymous user to the registered user.
        /// </summary>
        /// <param name="newUid">The User Id to migrate the card items to</param>
        /// <returns></returns>
        public ActionResult MigrateCart(Guid newUid)
        {
            var currentId = MvcApplication.Current.GetCurrentUserId();
            //Perform the migration in Catfish
            API.CreateClient().User.Migrate(currentId, newUid);
            MvcApplication.Current.SetCurrentUserId(newUid);

            return RedirectToAction("Index");
        }
    }
}