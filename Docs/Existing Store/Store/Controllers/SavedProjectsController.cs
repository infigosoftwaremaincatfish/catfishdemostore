﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store.Controllers
{
    public class SavedProjectsController : Controller
    {
        //
        // GET: /SavedProjects/

        public ActionResult Index()
        {
            //Load projects for user from catfish
            IEnumerable<string> projectIds =
                API.CreateClient().Orderline.GetSavedProjectItemsForUser(MvcApplication.Current.GetCurrentUserId());

            //Load the project info
            var orderlineitemInfos = projectIds.Select(item => API.CreateClient().Orderline.GetOrderLineItemInfo(item));

            var savedProjects = orderlineitemInfos
                .Select(current => new CartItem()
                {
                    Name = current.Name,
                    LineItemId = current.Id,
                    ProductId = current.ProductId,
                    Price = current.Price,
                    UserId = MvcApplication.Current.GetCurrentUserId(),
                    Thumbnail = current.ThumbnailUrls != null ? current.ThumbnailUrls.FirstOrDefault() : string.Empty
                }
                );

            return View(savedProjects);
        }

        //
        // POST: /Cart/Delete/5

        /// <summary>
        /// Deletes the order line item/saved project
        /// </summary>
        /// <param name="id">The id of the order line item to delete</param>
        /// <returns></returns>
        [ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            API.CreateClient().Orderline.DeleteOrderLineItem(id);

            return RedirectToAction("Index");
        }

    }
}