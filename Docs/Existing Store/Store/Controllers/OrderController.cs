﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CatfishApiSharp.Domain.OrderClient;
using CatfishApiSharp.Domain.OrderLine;

namespace Store.Controllers
{
    public class OrderController : Controller
    {
        //
        // GET: /Order/

        public ActionResult Index()
        {
            var orderIds = API.CreateClient().Order.GetOrderListForUser(MvcApplication.Current.GetCurrentUserId(), null);

            var orders = orderIds.Select(item => API.CreateClient().Order.GetOrderInfo(item));
            return View(orders);
        }

        public ActionResult SetPurchaseOrderNumber(int id, string purchaseOrderNumber)
        {
            API.CreateClient().Order.SetPurchaseOrderNumber(id, purchaseOrderNumber);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult SearchByNotes(string searchpattern)
        {
            var result = API.CreateClient().Orderline.SearchByNotes(searchpattern);
            return View(result); 
        }

        [HttpPost]
        public ActionResult SetItemNotes(string id, string notes)
        {
            API.CreateClient().Orderline.SetNotes(id, notes);
            return RedirectToAction("Index");
        }

        public ActionResult ReorderItem(string id)
        {
            API.CreateClient().Orderline.Reorder(id);
            return RedirectToAction("Index", "Cart");
        }

        public ActionResult Reorder(int id)
        {
            API.CreateClient().Order.Reorder(id);
            return RedirectToAction("Index", "Cart");
        }

        /// <summary>
        /// Display detailed info for the Order.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ActionResult Details(int id)
        {
            var orders = API.CreateClient().Order.GetOrderInfo(id);
            var orderItems = orders.OrderLineItems.Select(item => API.CreateClient().Order.GetOrderLineItemInfo(item)).ToList();
            ViewBag.OrderId = id;

            return View(orderItems);
        }


        /// <summary>
        /// Sets the status of the order. The status can only be in one direction (Open -> Paid -> Shipped -> Complete)
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        public ActionResult SetStatus(int id, int status)
        {
            API.CreateClient().Order.ChangeOrderStatus(id, (OrderItem.OrderStatus)status);
            return RedirectToAction("Index", new { id = id });
        }

        /// <summary>
        /// Sets the orderline status. (Only Verified, Shipped, OnHold, Printed, InReprographics, InProduction, Inpackagin, InFinishing, InArtwork, AwaitingProof, AwaitingParts, AwaitingData)
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="status">The status.</param>
        /// <param name="orderId">The order identifier.</param>
        /// <returns></returns>
        public ActionResult SetOrderlineStatus(string id, int status, int orderId)
        {
            API.CreateClient().Order.UpdateOrderLineItemInfo(id, (OrderLineItem.ItemStatus)status);
            return RedirectToAction("Details", new { id = orderId });
        }
    }
}