﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store.Controllers
{
    public class CategoryController : Controller
    {
        //
        // GET: /Category/

        public ActionResult Index()
        {
            return View(MvcApplication.Categories);
        }

        //
        // GET: /Category/Details/5

        public ActionResult Details(int id)
        {
            Category category = MvcApplication.Categories.SingleOrDefault(c => c.CategoryId == id);

            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        /// <summary>
        /// Reloads the catalog from Catfish.
        /// </summary>
        /// <returns></returns>
        public ActionResult ReloadCatalog()
        {
            new CategoryService().UpdateFromCatfish();
            return RedirectToAction("Index");
        }
    }
}