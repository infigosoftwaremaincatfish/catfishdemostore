﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Store.Models;
using CatfishApiSharp.Domain.Printing;
using System.IO;
using System.Configuration;
using System.Globalization;
using CatfishApiSharp;
using System.Security.Cryptography;
using System.Diagnostics;

namespace Store.Controllers
{
    public class CatfishSyncController : Controller
    {
        private ICatfishApiClient _client;
        private const string TestData = "CatfishSyncTestData";
        //
        // GET: /CatfishSync/

        public CatfishSyncController()
        {
            _client = API.CreateClient();
        }


        public ActionResult Index()
        {
            var data = TempData[TestData] as CatfishSyncTestData;

            var printLocations = API.CreateClient().Printing.AllPrintLocations();
            ViewBag.PrintLocations = printLocations.Select(pl => pl.Name);

            return View(data);
        }

        private void ValidateDownloadFileHash(FileStream fileContents, string fileContentHashValidate, int jobId)
        {
            string fileContentHash;

            using (var sha1Crypto = new SHA1CryptoServiceProvider())
            {
                fileContentHash = BitConverter.ToString(sha1Crypto.ComputeHash(fileContents));
            }

            if (string.IsNullOrEmpty(fileContentHashValidate) || fileContentHashValidate == fileContentHash)
                _client.Printing.SetOrderAsVerified(jobId);
        }

        public ActionResult DownloadTest(string printLocationName)
        {
            CatfishSyncTestData testData = new CatfishSyncTestData()
            {
                Test = "Download files test"
            };
            List<string> messages = new List<string>();
            testData.Messages = messages;
            TempData[TestData] = testData;

            var downloadLocation = ConfigurationManager.AppSettings["CatfishSyncDownloadFolder"];
            if (string.IsNullOrEmpty(downloadLocation) || !Directory.Exists(downloadLocation))
            {
                messages.Add("No valid download location setup (or does not exist): " + downloadLocation);
                return RedirectToAction("Index");
            }
           
            //Now perform the test similar to catfish sync

            PrintLocation printLocation;
            try
            {
                printLocation = _client.Printing.GetPrintLocationByName(printLocationName);
                messages.Add("Print location retrieved: " + printLocation.Name + " (" + printLocation.Id + ")");
            }
            catch (Exception ex)
            {
                messages.Add(string.Format("ERROR: Unable to get printlocation {0}", ex.InnerException == null ? ex.Message : ex.InnerException.Message));
                return RedirectToAction("Index");
            }

            /* Never a need to expose that and honestly no good idea
             * StorageFolder hotfolderStorage;
            try
            {
                hotfolderStorage = _client.GetStorageFolderByType("hotfolder");
            }
            catch (Exception)
            {
                Logger.Error("ERROR: Unable to get storage folder");
                throw;
            }
            */

            if (printLocation == null)
            {
                messages.Add(string.Format("INFORMATION!: No print location found with name {0}", printLocationName));
                return RedirectToAction("Index");
            }

            List<HotFolder> hotFolders;

            try
            {
                hotFolders = _client.Printing.GetHotFoldersByPrintLocationId(printLocation.Id.ToString(CultureInfo.InvariantCulture));
                foreach (var hotFolder in hotFolders)
                {
                    messages.Add("Returned hot folder: " + hotFolder.Name);
                }
            }
            catch (Exception ex)
            {
                messages.Add(string.Format("ERROR: Unable to get hot folders {0}", ex.InnerException == null ? ex.Message : ex.InnerException.Message));
                return RedirectToAction("Index");
            }

            foreach (var hotFolder in hotFolders)
            {

                //Change in api code - folde can just be appended to download location


                var folderName = Path.Combine(downloadLocation, hotFolder.Folder);

                try
                {
                    if (!Directory.Exists(folderName))
                    {
                        Directory.CreateDirectory(folderName);
                    }
                }
                catch (Exception)
                {
                    messages.Add(string.Format(string.Format("Unable to create folder {0}", folderName)));                    
                    return RedirectToAction("Index");
                }

                try
                {
                    var job = _client.Printing.GetNextDownloadJob(hotFolder.Id.ToString(CultureInfo.InvariantCulture));

                    var stopWatch = new Stopwatch();
                    //changed from while - as we do only one at a time for testing
                    if (job != null)
                    {
                        stopWatch.Reset();
                        try
                        {
                            messages.Add(string.Format("Downloading File {0} to {1}. Size: {2} bytes", job.FileName, folderName, job.FileSize));

                            stopWatch.Start();
                            _client.Printing.DownloadFile(job.DownloadId, string.Format("{0}\\{1}", folderName, job.FileName));
                            stopWatch.Stop();
                            messages.Add(string.Format("File {0} took {1} to download", job.FileName, stopWatch.Elapsed));
                            _client.Printing.SetJobAsDownloaded(job.DownloadId);

                            //hopefully the file format hasnt changed!!!
                            try
                            {
                                var iopId = Convert.ToInt32(job.FileName.Split('_')[2]);
                                _client.Printing.SetOrderAsDownloaded(iopId);
                            }
                            catch (Exception ex)
                            {
                                messages.Add(string.Format("Failed to set order as downloaded, {0} ", ex.Message));
                            }
                            try
                            {
                                var iopId = Convert.ToInt32(job.FileName.Split('_')[2]);
                                var fileStream = new FileStream(string.Format("{0}\\{1}", folderName, job.FileName), FileMode.Open);
                                ValidateDownloadFileHash(fileStream, job.IntegrityVerification, iopId);
                            }
                            catch (Exception ex)
                            {
                                messages.Add(string.Format("Failed to set order as verified, {0} ", ex.Message));
                            }
                        }
                        catch (Exception ex)
                        {
                            //Update the status to failed for the download job
                            messages.Add(string.Format("Failed to download file {0} to {1}", job.FileName, folderName));
                            messages.Add(string.Format("Error is {0}", ex.Message));
                            try
                            {
                                _client.Printing.SetJobAsFailedToDownload(job.DownloadId);
                            }
                            catch (Exception)
                            {
                                messages.Add(string.Format("Failed to set job as not downloaded. Error is {0}", ex.Message));
                                throw;
                            }

                        }
                        //Update the status of the job to success.

                        //Not needed as there is no loop
                        //job = _client.GetNextDownloadJob(hotFolder.Id.ToString(CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        messages.Add("No job available");
                    }
                }
                catch (Exception ex)
                {
                    messages.Add(string.Format("INFORMATION!: No Jobs found for hotfolder {0}. {1}", hotFolder.Name,ex.Message));
                }
            }            

            return RedirectToAction("Index");
        }
    }
}
