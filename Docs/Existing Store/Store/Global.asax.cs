﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Store.Controllers;

namespace Store
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        // 'DB' to hold a local copy of the catalogue
        public static List<Category> Categories = new List<Category>();
        public static List<Product> Products = new List<Product>();

        public static MvcApplication Current
        {
            get
            {
                return HttpContext.Current.ApplicationInstance as MvcApplication;
            }
        }

        public Guid GetCurrentUserId()
        {
            return (Guid)Session["UserId"];
        }

        public void SetCurrentUserId(Guid guid)
        {
            HttpContext.Current.Session.Add("UserId", guid);
        }

        protected void Session_Start()
        {
            SetCurrentUserId(Guid.NewGuid());
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            LoadCatalogue();
        }

        private void LoadCatalogue()
        {
            new CategoryService().UpdateFromCatfish();
        }
    }
}