﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CatfishApiSharp;
using System.Configuration;

namespace Store
{

    public class API
    {

        /// <summary>
        /// Creates the client connection to catfish.
        /// </summary>
        /// <returns></returns>
        public static ICatfishApiClient CreateClient()
        {
            //Load settings from config file
            var platformUrl = URL;
            var apiKey = ConfigurationManager.AppSettings["CatfishApiToken"];

            //connect to catfish
            CatfishApiSharp.ICatfishApiClient client = new CatfishApiSharp.CatfishApiClient(platformUrl, false, apiKey, string.Empty);
            return client;
        }

        /// <summary>
        /// Gets the Catfish URL.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        public static string URL { get { return ConfigurationManager.AppSettings["CatfishWebSite"]; } }
    }
}