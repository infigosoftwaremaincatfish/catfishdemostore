﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Store.Controllers;

namespace Store
{
    public class CategoryService
    {

        /// <summary>
        /// Updates the local catalogue from catfish.
        /// </summary>
        public void UpdateFromCatfish()
        {
            //TODO: Load recursive!!!

            // Get Categories
            var catfishCategoryIds = API.CreateClient().Catalog.GetCategoryList();
            MvcApplication.Categories = new List<Category>();
            MvcApplication.Products = new List<Product>();

            //Run through the categories and load them into the local 'db'
            foreach (var catfishCategoryId in catfishCategoryIds)
            {
                //Get the detailed category info from Catfish
                var catfishCat = API.CreateClient().Catalog.GetCategoryInfo(catfishCategoryId);
                var cat = new Category()
                {
                    Name = catfishCat.Name,
                    Description = catfishCat.Description,
                    CategoryId = catfishCat.Id,
                    Products = new List<Product>()
                };

                //Load the products for this category and save them in the 'db'
                var productIds = API.CreateClient().Catalog.GetProductsInCategory(catfishCategoryId);
                foreach (var productId in productIds)
                {
                    //Get the detailed product info from Catfish
                    var catfishProduct = API.CreateClient().Catalog.GetProductInfo(productId);
                    var product = new Product()
                    {
                        ProductId = catfishProduct.Id,
                        ProductArtUrl = catfishProduct.PreviewUrls.FirstOrDefault() ?? string.Empty,
                        Name = catfishProduct.Name,
                        Price = catfishProduct.Price
                    };
                    cat.Products.Add(product);
                    MvcApplication.Products.Add(product);
                }

                MvcApplication.Categories.Add(cat);
            }
        }
    }
}