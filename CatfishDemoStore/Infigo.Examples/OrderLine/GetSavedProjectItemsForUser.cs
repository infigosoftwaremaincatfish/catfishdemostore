﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CatfishApiSharp;

namespace Infigo.Examples.OrderLine
{
    class GetSavedProjectItemsForUser
    {
        static void Main_GetSavedProjectItemsForUser()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                Guid userId = Guid.NewGuid();

                // Get saved project for a specific user
                IEnumerable<string> projects = catfishApiClient.Orderline.GetSavedProjectItemsForUser(userId);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
