﻿using System;
using System.Configuration;
using CatfishApiSharp;

namespace Infigo.Examples.OrderLine
{
    class UpdateOrderLineItemQuantity
    {
        static void Main_UpdateOrderLineItemQuantity()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                string orderLineItem = "x1";
                int quantity = 10;

                // Set notes for a specific order line item
                catfishApiClient.Orderline.UpdateOrderLineItemQuantity(orderLineItem, quantity);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
