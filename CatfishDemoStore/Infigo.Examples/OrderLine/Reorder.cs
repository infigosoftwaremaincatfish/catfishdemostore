﻿using System;
using System.Configuration;
using CatfishApiSharp;

namespace Infigo.Examples.OrderLine
{
    class Reorder
    {
        static void Main_Reorder()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                string orderLineItemId = "x1";

                // Reorder an order line item id
                catfishApiClient.Orderline.Reorder(orderLineItemId);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
