﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.OrderLine;

namespace Infigo.Examples.OrderLine
{
    class SearchByNotes
    {
        static void Main_SearchByNotes()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                string notes = "Test note";

                // Search OrderLine items by notes
                IEnumerable<OrderLineItem> orderItemIds = catfishApiClient.Orderline.SearchByNotes(notes);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
