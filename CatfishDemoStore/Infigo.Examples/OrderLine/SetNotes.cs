﻿using System;
using System.Configuration;
using CatfishApiSharp;

namespace Infigo.Examples.OrderLine
{
    class SetNotes
    {
        static void Main_SetNotes()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                string orderLineItem = "x1";
                string notes = "Test Note";

                // Set notes for a specific order line item
                catfishApiClient.Orderline.SetNotes(orderLineItem, notes);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
