﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.OrderLine;

namespace Infigo.Examples.OrderLine
{
    class ListOrderLineItemIds
    {
        static void Main_ListOrderLineItemIds()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                int itemType = (int) OrderLineItem.ItemType.MultiPart;

                // Get order line items Ids per Item Type
                IEnumerable<string> item = catfishApiClient.Orderline.ListOrderLineItemIds(itemType);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
