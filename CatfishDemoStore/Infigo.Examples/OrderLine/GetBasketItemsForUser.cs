﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.OrderLine;

namespace Infigo.Examples.OrderLine
{
    class GetBasketItemsForUser
    {
        static void Main_GetBasketItemsForUser()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                Guid userId = Guid.NewGuid();

                // Get the list of existing OrderLine items for a specific user
                IEnumerable<string> orderItemIds = catfishApiClient.Orderline.GetBasketItemsForUser(userId);

                foreach (var id in orderItemIds)
                {
                    // get details about each found order line item
                    OrderLineItem orderLineItem = catfishApiClient.Orderline.GetOrderLineItemInfo(id);

                    Console.WriteLine("Id: {0}; Name: {1}", orderLineItem.Id, orderLineItem.Name);
                }
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
