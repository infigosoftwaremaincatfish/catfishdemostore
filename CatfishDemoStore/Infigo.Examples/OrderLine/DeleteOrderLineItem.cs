﻿using System;
using System.Configuration;
using CatfishApiSharp;

namespace Infigo.Examples.OrderLine
{
    class DeleteOrderLineItem
    {
        static void Main_DeleteOrderLineItem()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                // Remove a specific OrderLine Item from the system
                string id = "x1";

                catfishApiClient.Orderline.DeleteOrderLineItem(id);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
