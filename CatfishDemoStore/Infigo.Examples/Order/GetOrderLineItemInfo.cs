﻿using System;
using System.Configuration;
using CatfishApiSharp;

namespace Infigo.Examples.Order
{
    class GetOrderLineItemInfo
    {
        static void Main_GetOrderLineItemInfo()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                var orderLineId = "1x";
                CatfishApiSharp.Domain.OrderLine.OrderLineItem orderLineItem =
                    catfishApiClient.Order.GetOrderLineItemInfo(orderLineId);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
