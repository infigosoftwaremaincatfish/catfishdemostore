﻿using System;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.OrderLine;

namespace Infigo.Examples.Order
{
    class UpdateOrderLineItemInfo
    {
        static void Main_UpdateOrderLineItemInfo()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                string orderLineItemId = "x1";
                var status = OrderLineItem.ItemStatus.Approved;

                // update order line item details by specifying the id
                catfishApiClient.Order.UpdateOrderLineItemInfo(orderLineItemId, (OrderLineItem.ItemStatus)status);

                var orderLineItem = new CatfishApiSharp.Domain.OrderLine.OrderLineItem();

                // update order line item details by specifying the OrderLineItem object
                catfishApiClient.Order.UpdateOrderLineItemInfo(orderLineItem, (OrderLineItem.ItemStatus)status);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
