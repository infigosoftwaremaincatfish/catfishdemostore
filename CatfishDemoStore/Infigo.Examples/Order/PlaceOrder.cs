﻿using System;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.Common;

namespace Infigo.Examples.Order
{
    class PlaceOrder
    {
        static void Main_PlaceOrder()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                Guid userId = Guid.NewGuid();
                var shippingAddress = new Address();
                var billingAddress = new Address();
                var shippingMethod = "DHL";
                bool isPaid = true;

                var orderId = catfishApiClient.Order.PlaceOrder(userId, shippingAddress, billingAddress, shippingMethod, isPaid);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
