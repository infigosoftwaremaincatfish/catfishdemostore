﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.OrderClient;

namespace Infigo.Examples.Order
{
    class GetOrderInfo
    {
        static void Main_GetOrderInfo()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                // Get the order list of a specific user
                IEnumerable<int> orderIds = catfishApiClient.Order.GetOrderListForUser(Guid.NewGuid(), null);

                foreach (var id in orderIds)
                {
                    // get details about each found order id
                    OrderItem orderInfo = catfishApiClient.Order.GetOrderInfo(id);

                    Console.WriteLine("Id: {0}; OrderStatus: {1}", orderInfo.Id, orderInfo.Status);
                }
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
