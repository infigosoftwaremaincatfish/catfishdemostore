﻿using System;
using System.Configuration;
using CatfishApiSharp;

namespace Infigo.Examples.Order
{
    class Reorder
    {
        static void Main_Reorder()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                int orderId = 12345;
                catfishApiClient.Order.Reorder(orderId);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
