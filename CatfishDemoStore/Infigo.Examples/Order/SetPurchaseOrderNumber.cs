﻿using System;
using System.Configuration;
using CatfishApiSharp;

namespace Infigo.Examples.Order
{
    class SetPurchaseOrderNumber
    {
        static void Main_SetPurchaseOrderNumber()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                int orderId = 12345;
                string purchaseOrderNumber = "MB123456789";
                // set purchase order number of a specific order
                catfishApiClient.Order.SetPurchaseOrderNumber(orderId, purchaseOrderNumber);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
