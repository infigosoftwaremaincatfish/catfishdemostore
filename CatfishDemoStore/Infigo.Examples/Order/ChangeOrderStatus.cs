﻿using System;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.Catalog;
using CatfishApiSharp.Domain.OrderClient;

namespace Infigo.Examples.Order
{
    class ChangeOrderStatus
    {
        static void Main_ChangeOrderStatus()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                /*
                 * OrderStatus:
                 * Complete
                 * Open
                 * Paid
                 * Shipped
                 */
                var orderId = 1;
                catfishApiClient.Order.ChangeOrderStatus(orderId, OrderItem.OrderStatus.Complete);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
