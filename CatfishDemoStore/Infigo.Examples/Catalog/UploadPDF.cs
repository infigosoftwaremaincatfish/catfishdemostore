﻿using System;
using System.Configuration;
using System.IO;
using CatfishApiSharp;

namespace Infigo.Examples.Catalog
{
    class UploadPdf
    {
        static void Main_UploadPdf()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            const int productId = 1;
            FileInfo pdfFileInfo = new FileInfo(@"c:\test.pdf");

            try
            {
                catfishApiClient.Catalog.UploadPdf(productId, pdfFileInfo.FullName);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
