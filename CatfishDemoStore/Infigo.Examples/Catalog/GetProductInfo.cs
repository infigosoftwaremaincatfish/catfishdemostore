﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.Catalog;

namespace Infigo.Examples.Catalog
{
    class GetProductInfo
    {
        static void Main_GetProductInfo()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                // Get the list of existing product Ids
                IEnumerable<int> productIds = catfishApiClient.Catalog.GetProductList();

                foreach (var id in productIds)
                {
                    // get details about each found product id
                    ProductInfo productInfo = catfishApiClient.Catalog.GetProductInfo(id);

                    Console.WriteLine("Name: {0}; ShortDescription: {1}", productInfo.Name, productInfo.ShortDescription);
                }
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
