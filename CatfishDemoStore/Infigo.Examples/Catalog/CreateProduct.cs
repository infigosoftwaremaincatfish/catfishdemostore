﻿using System;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.Catalog;

namespace Infigo.Examples.Catalog
{
    class CreateProduct
    {
        static void Main_CreateProduct()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                var newProductId = catfishApiClient.Catalog.CreateProduct(new CreateProductRequest
                {
                    Name = "Example Product",
                    Description = "Some description",
                    CategoryIds = new[] { 1, 2, 3 },
                    Price = 10,
                    CreateFromProductId = null, // you can provide a product id from where to copy existing fields
                    Tags = new[] { "Tag1", "Tag2" },
                    TierPrices =
                        {
                            new TierPrice
                                {
                                    Price = 100,
                                    Quantity = 10
                                },
                            new TierPrice
                                {
                                    Price = 170,
                                    Quantity = 20
                                }
                        }
                });
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
