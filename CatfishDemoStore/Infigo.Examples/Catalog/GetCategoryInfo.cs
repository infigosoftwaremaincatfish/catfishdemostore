﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.Catalog;

namespace Infigo.Examples.Catalog
{
    class GetCategoryInfo
    {
        static void Main_GetCategoryInfo()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                // Get the list of existing category Ids
                IEnumerable<int> categoryIds = catfishApiClient.Catalog.GetCategoryList();

                foreach (var id in categoryIds)
                {
                    // get details about each found category
                    CategoryInfo categoryInfo = catfishApiClient.Catalog.GetCategoryInfo(id);

                    Console.WriteLine("Name: {0}; Description: {1}", categoryInfo.Name, categoryInfo.Description);
                }
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
