﻿using System;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.Catalog;

namespace Infigo.Examples.Catalog
{
    class CreateCategory
    {
        static void Main_CreateCategory()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                var newCategoryId = catfishApiClient.Catalog.CreateCategory(new CreateCategoryRequest
                {
                    Name = "Example Category",
                    Description = "Some description",
                    ParentCategoryId = null // you can provide parent category id if required
                });

                // you can get now newly created category details
                CategoryInfo categoryInfo = catfishApiClient.Catalog.GetCategoryInfo(newCategoryId);

                Console.WriteLine(categoryInfo.Name);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
