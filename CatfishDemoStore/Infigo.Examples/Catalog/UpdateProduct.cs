﻿using System;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.Catalog;

namespace Infigo.Examples.Catalog
{
    class UpdateProduct
    {
        static void Main_UpdateProduct()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);
            const int productIdToUpdate = 1;

            try
            {
                var result = catfishApiClient.Catalog.UpdateProduct(new UpdateProductRequest
                {
                    Name =  "Updated product name",
                    CategoryIds = new []{1,2,3},
                    Description = "Updated description",
                    Price = 20,
                    ProductId = productIdToUpdate, // The product we want to update
                    Tags = new []{ "Tag1", "Tag2"}
                });

                Console.WriteLine(result ? "Successfully updated product {0}" : "Failed to update product {0}",
                    productIdToUpdate);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
