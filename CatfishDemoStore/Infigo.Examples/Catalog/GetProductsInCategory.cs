﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.Catalog;

namespace Infigo.Examples.Catalog
{
    class GetProductsInCategory
    {
        static void Main_GetProductsInCategory()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                // Get the list of existing category Ids
                IEnumerable<int> categoryIds = catfishApiClient.Catalog.GetCategoryList();

                foreach (var id in categoryIds)
                {
                    // get products ids list for category id
                    IEnumerable<int> productIds = catfishApiClient.Catalog.GetProductsInCategory(id);

                    foreach (var pid in productIds)
                    {
                        // get details about each found product id
                        ProductInfo productInfo = catfishApiClient.Catalog.GetProductInfo(pid);

                        Console.WriteLine("1. Name: {0}; ShortDescription: {1}", productInfo.Name, productInfo.ShortDescription);
                    }

                    // get products ids list for CategoryInfo
                    CategoryInfo categoryInfo = catfishApiClient.Catalog.GetCategoryInfo(id);
                    IEnumerable<int> productIds2 = catfishApiClient.Catalog.GetProductsInCategory(categoryInfo);

                    foreach (var pid in productIds2)
                    {
                        // get details about each found product id
                        ProductInfo productInfo = catfishApiClient.Catalog.GetProductInfo(pid);

                        Console.WriteLine("2. Name: {0}; ShortDescription: {1}", productInfo.Name, productInfo.ShortDescription);
                    }
                }
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
