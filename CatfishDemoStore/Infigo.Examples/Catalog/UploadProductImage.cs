﻿using System;
using System.Configuration;
using System.IO;
using CatfishApiSharp;

namespace Infigo.Examples.Catalog
{
    class UploadProductImage
    {
        static void Main_UploadProductImage()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            const int productId = 1;
            FileInfo thumbnailFileInfo = new FileInfo(@"c:\testThumbnail.png");
            const bool clearExistingThumbnail = false;

            try
            {
                catfishApiClient.Catalog.UploadProductImage(productId, thumbnailFileInfo.FullName, clearExistingThumbnail);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
