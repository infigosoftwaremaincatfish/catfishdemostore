﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.Printing;

namespace Infigo.Examples.Printing
{
    class GetHotFoldersByPrintLocationId
    {
        static void Main_GetHotFoldersByPrintLocationId()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);
            const string printLocationId = "1";
            try
            {
                List<HotFolder> hotfolderList = catfishApiClient.Printing.GetHotFoldersByPrintLocationId(printLocationId);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
