﻿using System;
using System.Configuration;
using CatfishApiSharp;

namespace Infigo.Examples.Printing
{
    class SetJobAsFailedToDownload
    {
        static void Main_SetJobAsFailedToDownload()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);
            const string hotFolderId = "1";

            try
            {
                var job = catfishApiClient.GetNextDownloadJob(hotFolderId);
                catfishApiClient.Printing.SetJobAsFailedToDownload(job.DownloadId);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
