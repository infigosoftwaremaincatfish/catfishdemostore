﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.Printing;

namespace Infigo.Examples.Printing
{
    class GetPrintLocationByName
    {
        static void Main_GetPrintLocationByName()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);
            const string printLocationName = "TestName";

            try
            {
                PrintLocation printLocation = catfishApiClient.Printing.GetPrintLocationByName(printLocationName);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
