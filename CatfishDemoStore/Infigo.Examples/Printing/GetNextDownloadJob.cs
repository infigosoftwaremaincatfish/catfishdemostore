﻿using System;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain;

namespace Infigo.Examples.Printing
{
    class GetNextDownloadJob
    {
        static void Main_GetNextDownloadJob()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);
            const string hotFolderId = "1";
            const string folderName = @"C:\Temp";

            try
            {
                DownloadJob job = catfishApiClient.GetNextDownloadJob(hotFolderId);

                catfishApiClient.Printing.DownloadFile(job.DownloadId, string.Format("{0}\\{1}", folderName, job.FileName));
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
