﻿using System;
using System.Configuration;
using CatfishApiSharp;

namespace Infigo.Examples.Printing
{
    class SetOrderAsVerified
    {
        static void Main_SetOrderAsVerified()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);
            const string hotFolderId = "1";

            try
            {
                var job = catfishApiClient.GetNextDownloadJob(hotFolderId);
                int infigoOrderProductId = Convert.ToInt32(job.FileName.Split('_')[2]);

                catfishApiClient.SetOrderAsVerified(infigoOrderProductId, "d");
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
