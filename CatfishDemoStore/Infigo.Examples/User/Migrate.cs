﻿using System;
using System.Configuration;
using CatfishApiSharp;

namespace Infigo.Examples.User
{
    class Migrate
    {
        static void Main_Migrate()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            var fromCustomer = new Guid("14031df6-7411-40e6-9c45-dc6612c0da70");
            var toCustomer = new Guid();

            try
            {
                // migrating data
                catfishApiClient.User.Migrate(fromCustomer, toCustomer);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
