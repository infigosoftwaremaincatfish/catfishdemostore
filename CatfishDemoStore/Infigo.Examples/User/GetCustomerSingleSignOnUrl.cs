﻿using System;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.User;

namespace Infigo.Examples.User
{
    class GetCustomerSingleSignOnUrl
    {
        static void Main_GetCustomerSingleSignOnUrl()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                // using the Guid
                CustomerSSOResult customerSSOResult1 = catfishApiClient.User.GetCustomerSingleSignOnUrl(new Guid(), "/admin");
                Console.WriteLine("Generated url for admin site: {0}", customerSSOResult1.LoginUrl);

                // using the Guid as string
                CustomerSSOResult customerSSOResult2 = catfishApiClient.User.GetCustomerSingleSignOnUrl(new Guid().ToString());
                Console.WriteLine("Generated url for fron end site: {0}", customerSSOResult2.LoginUrl);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
