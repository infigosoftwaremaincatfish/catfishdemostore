﻿using System;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.User;

namespace Infigo.Examples.User
{
    class GetCustomerCookieToken
    {
        static void Main_GetCustomerCookieToken()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            try
            {
                // using the Guid
                CustomerToken customerToken1 = catfishApiClient.User.GetCustomerCookieToken(new Guid());
                // using the Guid as string
                CustomerToken customerToken2 = catfishApiClient.User.GetCustomerCookieToken(new Guid().ToString());
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
