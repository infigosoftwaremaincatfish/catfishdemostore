﻿using System;
using System.Configuration;
using CatfishApiSharp;
using CatfishApiSharp.Domain.User;

namespace Infigo.Examples.User
{
    class GetEditorLinkFor
    {
        static void Main_GetEditorLinkFor()
        {
            string catfishApiBaseAddress = ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            string catfishApiKey = ConfigurationManager.AppSettings["CatfishApiKey"];
            // create an instance of the CatfishApiClient
            var catfishApiClient = new CatfishApiClient(catfishApiBaseAddress, false, catfishApiKey, String.Empty);

            const int productId = 1;
            const string orderlineItemId = "10";

            try
            {
                // passing the product id only
                string editorLink1 = catfishApiClient.User.GetEditorLinkFor(new Guid(), productId);
                Console.WriteLine("Generated editor link url : {0}", editorLink1);

                // using the Guid as string
                string editorLink2 = catfishApiClient.User.GetEditorLinkFor(new Guid(), productId, orderlineItemId);
                Console.WriteLine("Generated editor link url : {0}", editorLink2);
            }
            catch (Exception ex)
            {
                // failed
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
