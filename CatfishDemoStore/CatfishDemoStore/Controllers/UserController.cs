﻿using System;
using System.Web.Mvc;
using CatfishDemoStore.Helper;
using CatfishDemoStore.Services;
using CatfishDemoStore.ViewModel;

namespace CatfishDemoStore.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            UserViewModel model =
                new UserViewModel
                {
                    CurrentUserId = SessionVariables.CurrentUserId,
                    CustomerToken = CatfishApi.GetCustomerCookieToken(SessionVariables.CurrentUserId)
                };

            return View(model);
        }

        /// <summary>
        /// Saves the user identifier for the session.
        /// </summary>
        /// <param name="guid">The unique identifier.</param>
        /// <returns></returns> 
        [HttpPost]
        public ActionResult ChangeUserId(Guid guid)
        {
            SessionVariables.CurrentUserId = guid;
            Global.Orders = null;
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Migrates the user from an anonymous to registered user and copies the cart items.
        /// </summary>
        /// <param name="guid">Registered users Guid</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult MigrateUser(Guid guid)
        {
            var currentId = SessionVariables.CurrentUserId;
            CatfishApi.MigrateDataToUser(currentId, guid);
            SessionVariables.CurrentUserId = guid;
            Global.Orders = null;

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Creates new customer based on email, username and password
        /// </summary>
        /// <returns></returns> 
        [HttpPost]
        public ActionResult CreateNewCustomer(string email, string username, string password)
        {
            CatfishApi.CreateCustomer(email, username, password);
            return RedirectToAction("Index");
        }
        
        /// <summary>
        /// Get Existing Customer by email
        /// </summary>
        /// <returns></returns> 
        [HttpPost]
        public ActionResult Details(string email)
        {
            var customer = CatfishApi.GetCustomerByEmail(email);
            if (customer == null) return RedirectToAction("Index");
            return View(customer);
        }

        /// <summary>
        /// Get SSO Urls for email
        /// </summary>
        /// <returns></returns> 
        public JsonResult GetCustomerSSOResultByEmail(string email)
        {
            var site = CatfishApi.GetCustomerSSOResultByEmail(email, null);
            var admin = CatfishApi.GetCustomerSSOResultByEmail(email, "/admin");
            if (site == null || admin == null) return Json("error", JsonRequestBehavior.AllowGet);
            var baseAdress = "http://" + Configuration.CatfishApiBaseAddress;
            return Json(new {
                siteSSOUrl = baseAdress + site.LoginUrl,
                adminSSOUrl = baseAdress + admin.LoginUrl
                }, JsonRequestBehavior.AllowGet);
        }
        
        /// <summary>
        /// Get Customer TOken for email
        /// </summary>
        /// <returns></returns> 
        public JsonResult GetCustomerTokenByEmail(string email)
        {
            var token = CatfishApi.GetCustomerTokenByEmail(email);
            if (token == null) return Json("error", JsonRequestBehavior.AllowGet);
            
            return Json(new {
                cookieName = token.CookieName,
                token = token.Token
                }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get's the logonURL and redirects the user to it to log them into both stores
        /// </summary>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns></returns>
        public ActionResult LogOnOtherSite(string returnUrl = null)
        {
            return Redirect(CatfishApi.GetCustomerSingleSignOnUrl(SessionVariables.CurrentUserId, returnUrl));
        }
    }
}