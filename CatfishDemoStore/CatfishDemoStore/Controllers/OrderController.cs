﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CatfishApiSharp.Domain.OrderClient;
using CatfishApiSharp.Domain.OrderLine;
using CatfishDemoStore.Helper;
using CatfishDemoStore.Models;
using CatfishDemoStore.Services;

namespace CatfishDemoStore.Controllers
{
    public class OrderController : Controller
    {
        // GET: Order
        public ActionResult Index()
        {
            var orders = CatfishApi.GetOrderListForUser(SessionVariables.CurrentUserId, null);
            return View(orders ?? new List<OrderItem>());
        }

        public ActionResult Details(int id)
        {
            var order = new Order(CatfishApi.GetOrderItem(id));
            order.Products = order.OrderLineItems.Select(CatfishApi.GetOrderLineDetails).ToList();
            ViewBag.OrderId = id;
            return View(order);
        }
        public ActionResult SetPurchaseOrderNumber(int id, string purchaseOrderNumber)
        {
            CatfishApi.SetPurchaseOrderNumber(id, purchaseOrderNumber);
            return RedirectToAction("Details", new { id });
        }

        [HttpPost]
        public ActionResult SearchByNotes(string searchpattern)
        {
            var orders = CatfishApi.SearchByNotes(searchpattern);
            return View(orders ?? new List<OrderLineItem>());
        }

        public ActionResult Reorder(int id)
        {
            CatfishApi.Reorder(id);
            return RedirectToAction("Index", "Cart");
        }

        public ActionResult ReorderItem(string id)
        {
            CatfishApi.ReorderItem(id);
            return RedirectToAction("Index", "Cart");
        }

        [HttpPost]
        public JsonResult SetItemNotes(string id, string notes)
        {
            CatfishApi.SetOrderLineItemNotes(id, notes);
            return Json("success", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Sets the status of the order. The status can only be in one direction (Open -> Paid -> Shipped -> Complete)
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        public JsonResult SetStatus(int id, int status)
        {
            CatfishApi.ChangeOrderStatus(id, (OrderItem.OrderStatus) status);
            return Json("success", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Sets the order line status. (Only Verified, Shipped, OnHold, Printed, InReprographics, InProduction, Inpackagin, InFinishing, InArtwork, AwaitingProof, AwaitingParts, AwaitingData)
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="status">The status.</param>
        /// <param name="orderId">The order identifier.</param>
        /// <returns></returns>
        public ActionResult SetOrderlineStatus(string id, int status, int orderId)
        {
            CatfishApi.UpdateOrderLineItemStatus(id, status);
            return RedirectToAction("Details", new {id = orderId});
        }
    }
}