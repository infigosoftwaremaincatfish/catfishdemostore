﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using CatfishDemoStore.Helper;
using CatfishDemoStore.Models;
using CatfishDemoStore.Models.Catfish;
using CatfishDemoStore.Services;
using CatfishDemoStore.ViewModel;

namespace CatfishDemoStore.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            return View(Global.Products ?? new List<Product>());
        }

        public ActionResult Edit(int id = 0)
        {
            string editorUrl;
            try
            {
                editorUrl = "http://" + Configuration.CatfishApiBaseAddress + CatfishApi.CatfishApiClient.User.GetEditorLinkFor(SessionVariables.CurrentUserId, id);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Could not load editor url", ex);
                return RedirectToAction("Index");
            }
            
            ViewBag.EditorUrl = editorUrl;

            return View();
        }

        public ActionResult Details(int id = 0)
        {
            var productInfo = CatfishApi.GetProduct(id);

            return View(productInfo);
        }

        public ActionResult UpdateDetails(int id)
        {
            var productInfo = CatfishApi.GetProduct(id);
            return View(productInfo);
        }

        public ActionResult UpdateProductDetails(int id, string name, string shortDescription, decimal? price)
        {

            CatfishApi.UpdateProduct(id, name, shortDescription, price);
            // refresh the lists
            Global.Categories = null;
            Global.Products = null;

            return RedirectToAction("Index");
        }

        public ActionResult Add(int? parentProductId)
        {
            return View(new ProductViewModel
            {
                ParentProductId = parentProductId,
                Categories = Global.Categories ?? new List<Category>()
            });
        }

        [HttpPost]
        public ActionResult AddProduct(string name, string description, decimal? price, int[] categories, int? parentProductId)
        {
            CatfishApi.CreateProduct(name, description, price, categories, parentProductId);
            // refresh the lists
            Global.Categories = null;
            Global.Products = null;

            return RedirectToAction("Index");
        }

        public ActionResult UploadThumbnailAndPdf(int id)
        {
            CatfishApi.UploadPdf(id, Request.MapPath("~/Content/Sample/PdfDocument.pdf"));
            CatfishApi.UploadProductImage(id, Request.MapPath("~/Content/Sample/Thumbnail.png"));
            // refresh the lists
            Global.Categories = null;
            Global.Products = null;

            return RedirectToAction("Index");
        }

        public ActionResult UploadVariablePdf(int id)
        {
            CatfishApi.UploadPdf(id, Request.MapPath("~/Content/Sample/Variable_PdfDocument.pdf"));
            // refresh the lists
            Global.Categories = null;
            Global.Products = null;

            return RedirectToAction("Index");
        }

        //
        // GET: /Product/Edit/v5
        public ActionResult EditExisting(string lineItemId, int productId)
        {
            try
            {
                //Get the editor link for the iframe contents
                ViewBag.EditorUrl = "http://" + Configuration.CatfishApiBaseAddress + CatfishApi.CatfishApiClient.User.GetEditorLinkFor(SessionVariables.CurrentUserId, productId, lineItemId);
            }
            catch (Exception ex)
            {
                
                LogHelper.Error("Could not generate Editor Link", ex);
            }
            
            return View("Edit");
        }
    }
}