﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web.Mvc;
using CatfishApiSharp.Domain;
using CatfishDemoStore.Services;
using CatfishDemoStore.ViewModel;
using DownloadJob = CatfishApiSharp.Domain.Printing.DownloadJob;
using HotFolder = CatfishApiSharp.Domain.HotFolder;
using PrintLocation = CatfishApiSharp.Domain.PrintLocation;

namespace CatfishDemoStore.Controllers
{
    public class CatfishSyncController : Controller
    {
        private const string TestData = "CatfishSyncTestData";
        // GET: CatfishSync
        public ActionResult Index(string name = null)
        {
            var printLocations = string.IsNullOrEmpty(name)
                ? CatfishApi.GetAllPrintLocations()
                : new List<PrintLocation> { CatfishApi.GetPrintLocationByName(name) };

            return View(printLocations ?? new List<PrintLocation>());
        }
        
        // GET: HotFolders
        public ActionResult HotFolders()
        {
            return View(CatfishApi.GetAllHotFolders() ?? new List<HotFolder>());
        }
        
        // GET: HotFoldersListForPrintLocation
        public ActionResult HotFoldersListForPrintLocation(string id)
        {
            return View(CatfishApi.GetHotFoldersByPrintLocationId(id) ?? new List<HotFolder>());
        }

        // GET: HotFolders
        public ActionResult UploadLocations(string name = null)
        {
            var uploadLocations = string.IsNullOrEmpty(name)
                ? CatfishApi.GetAllUploadLocations()
                : new List<UploadLocation> { CatfishApi.GetUploadLocationByName(name) };

            return View(uploadLocations ?? new List<UploadLocation>());
        }

        public ActionResult DownloadTest(string printLocationName)
        {
            ActionResult actionResult;

            List<MessageViewModel> messages = new List<MessageViewModel>();
            TempData[TestData] = messages;

            var downloadLocation = GetDownloadLocation(messages, out actionResult);
            if (actionResult != null) return actionResult;

            //Now perform the test similar to catfish sync
            var printLocation = GetPrinLocation(printLocationName, messages, out actionResult);
            if(actionResult != null) return actionResult;

            var hotFolders = GetHotFoldersList(printLocation, messages, out actionResult);
            if (actionResult != null) return actionResult;

            foreach (var hotFolder in hotFolders)
            {
                //Change in api code - folde can just be appended to download location
                var folderName = GetOrCreateFolder(downloadLocation, hotFolder, messages, out actionResult);
                if (actionResult != null) return actionResult;

                try
                {
                    var job = CatfishApi.GetNextDownloadJob(hotFolder.Id);

                    //changed from while - as we do only one at a time for testing
                    if (job != null)
                    {
                        try
                        {
                            AddMessage(messages, MessageType.Info, string.Format("Downloading File {0} to {1}. Size: {2} bytes", job.FileName, folderName, job.FileSize));
                            
                            // NOTE: using the CatfishApiClient explicitly as we need in case of exception to log a message
                            DownloadFile(job, folderName, messages);

                            CatfishApi.CatfishApiClient.SetJobAsDownloaded(job.DownloadId);

                            SetOrderAsDownloaded(job, messages);

                            ValidateDownloadFileHash(job, folderName, messages);
                        }
                        catch (Exception ex)
                        {
                            //Update the status to failed for the download job
                            AddMessage(messages, MessageType.Error, string.Format("Failed to download file {0} to {1}", job.FileName, folderName));
                            AddMessage(messages, MessageType.Error, string.Format("Error is {0}", ex.Message));
                            try
                            {
                                CatfishApi.SetJobAsFailedToDownload(job.DownloadId);
                            }
                            catch (Exception)
                            {
                                AddMessage(messages, MessageType.Error, string.Format("Failed to set job as not downloaded. Error is {0}", ex.Message));
                                throw;
                            }

                        }
                        //Update the status of the job to success.
                    }
                    else
                    {
                        AddMessage(messages, MessageType.Info, "No job available");
                    }
                }
                catch (Exception ex)
                {
                     AddMessage(messages, MessageType.Info, string.Format("INFORMATION!: No Jobs found for hotfolder {0}. {1}", hotFolder.Name, ex.Message));
                }
            }

            return RedirectToAction("Index");
        }

        private void ValidateDownloadFileHash(DownloadJob job, string folderName, List<MessageViewModel> messages)
        {
            try
            {
                var iopId = Convert.ToInt32(job.FileName.Split('_')[2]);
                var fileStream = new FileStream(string.Format("{0}\\{1}", folderName, job.FileName), FileMode.Open);
                string fileContentHash;

                using (var sha1Crypto = new SHA1CryptoServiceProvider())
                {
                    fileContentHash = BitConverter.ToString(sha1Crypto.ComputeHash(fileStream));
                }

                if (string.IsNullOrEmpty(job.IntegrityVerification) || job.IntegrityVerification == fileContentHash)
                    CatfishApi.CatfishApiClient.SetOrderAsVerified(iopId, "d");
            }
            catch (Exception ex)
            {
                AddMessage(messages, MessageType.Error, string.Format("Failed to set order as verified, {0} ", ex.Message));
            }
        }

        private static void SetOrderAsDownloaded(DownloadJob job, List<MessageViewModel> messages)
        {
            //hopefully the file format hasnt changed!!!
            try
            {
                var iopId = Convert.ToInt32(job.FileName.Split('_')[2]);
                CatfishApi.CatfishApiClient.SetOrderAsDownloaded(iopId, "d");
            }
            catch (Exception ex)
            {
                AddMessage(messages, MessageType.Error, string.Format("Failed to set order as downloaded, {0} ", ex.Message));
            }
        }

        private static void DownloadFile(DownloadJob job, string folderName, List<MessageViewModel> messages)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            CatfishApi.CatfishApiClient.DownloadFile(job.DownloadId, string.Format("{0}\\{1}", folderName, job.FileName));
            stopWatch.Stop();
            AddMessage(messages, MessageType.Info, string.Format("File {0} took {1} to download", job.FileName, stopWatch.Elapsed));
        }

        private string GetDownloadLocation(List<MessageViewModel> messages, out ActionResult actionResult)
        {
            var downloadLocation = Request.MapPath("~/Content/Download/");
            actionResult = null;

            if (string.IsNullOrEmpty(downloadLocation) || !Directory.Exists(downloadLocation))
            {
                AddMessage(messages, MessageType.Error, "No valid download location setup (or does not exist): " + downloadLocation);
                {
                    actionResult = RedirectToAction("Index");
                }
            }

            return downloadLocation;
        }

        private string GetOrCreateFolder(string downloadLocation, HotFolder hotFolder, List<MessageViewModel> messages, out ActionResult actionResult)
        {
            var folderName = Path.Combine(downloadLocation, hotFolder.Folder);
            actionResult = null;

            try
            {
                if (!Directory.Exists(folderName))
                {
                    Directory.CreateDirectory(folderName);
                }
            }
            catch (Exception)
            {
                AddMessage(messages, MessageType.Error, string.Format("Unable to create folder {0}", folderName));
                {
                    actionResult = RedirectToAction("Index");
                }
            }
            return folderName;
        }

        private List<HotFolder> GetHotFoldersList(PrintLocation printLocation, List<MessageViewModel> messages, out ActionResult actionResult)
        {
            List<HotFolder> hotFolders = null;
            actionResult = null;
            try
            {
                hotFolders =
                    CatfishApi.GetHotFoldersByPrintLocationId(printLocation.Id.ToString(CultureInfo.InvariantCulture))
                        .ToList();

                AddMessage(messages, MessageType.Info, hotFolders.Select(hotFolder => "Returned hot folder: " + hotFolder.Name));
            }
            catch (Exception ex)
            {
                AddMessage(messages, MessageType.Error, string.Format("ERROR: Unable to get hot folders {0}",
                    ex.InnerException == null ? ex.Message : ex.InnerException.Message));
                {
                    actionResult = RedirectToAction("Index");
                }
            }

            return hotFolders;
        }

        private PrintLocation GetPrinLocation(string printLocationName, List<MessageViewModel> messages, out ActionResult actionResult)
        {
            PrintLocation printLocation = null;
            actionResult = null;

            try
            {
                printLocation = CatfishApi.GetPrintLocationByName(printLocationName);
                AddMessage(messages, MessageType.Info, "Print location retrieved: " + printLocation.Name + " (" + printLocation.Id + ")");
            }
            catch (Exception ex)
            {
                AddMessage(messages, MessageType.Error, string.Format("ERROR: Unable to get printlocation {0}",
                    ex.InnerException == null ? ex.Message : ex.InnerException.Message));
                {
                    actionResult = RedirectToAction("Index");
                }
            }
            
            return printLocation;
        }

        private static void AddMessage(List<MessageViewModel> messages, MessageType msgType, string message)
        {
            messages.Add(new MessageViewModel{Message = message, MessageType = msgType});
        }
        private static void AddMessage(List<MessageViewModel> messages, MessageType msgType, IEnumerable<string> messageRange)
        {
            messages.AddRange(messageRange.Select(message => new MessageViewModel {Message = message, MessageType = msgType}));
        }
    }
}