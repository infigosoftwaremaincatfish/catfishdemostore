﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CatfishApiSharp.Domain.Common;
using CatfishApiSharp.Domain.OrderClient;
using CatfishDemoStore.Helper;
using CatfishDemoStore.Models;
using CatfishDemoStore.Services;

namespace CatfishDemoStore.Controllers
{
    public class CartController : Controller
    {
        // GET: Cart
        public ActionResult Index()
        {
            //Load the items in the users Catfish Basket
            var cartItemIds = CatfishApi.GetOrderLineIdsForUser(SessionVariables.CurrentUserId);
            cartItemIds = cartItemIds ?? new List<string>();

            try
            {
                //Load the details for each of the items
                var cartItems = cartItemIds.Select(CatfishApi.GetOrderLineDetails)
                    .Select(current => new CartItem
                    {
                        Name = current.Name,
                        LineItemId = current.Id,
                        ProductId = current.ProductId,
                        Price = current.Price,
                        UserId = SessionVariables.CurrentUserId,
                        Quantity = current.Quantity,
                        Thumbnail =
                            current.ThumbnailUrls != null ? current.ThumbnailUrls.FirstOrDefault() : string.Empty
                    });

                return View(cartItems);
            }
            catch (Exception ex)
            {
                LogHelper.Error("Error on Get CartItems", ex);
            }

            return View(new List<CartItem>());
        }

        /// <summary>
        /// Submit the items in the basket as an order.
        /// </summary>
        /// <returns></returns>
        public ActionResult Checkout()
        {
            //Hard coded address for testing. Needs to be taken from the page form in production
            var address = new Address
            {
                AddressLine1 = "My House",
                AddressLine2 = "My Road",
                Country = "Our Country",
                Email = "Some@web.site",
                FirstName = "My Name",
                LastName = "My LastName",
                StateProvince = "My County",
                Telephone = "01818 11 81 81",
                Town = "My Town",
                ZipPostalCode = "My Postcode"
            };

            //Place the order with Catfish
            var newOrderId = CatfishApi.PlaceOrder(SessionVariables.CurrentUserId, address);
            return RedirectToAction("CheckoutConfirm", new {newOrderId});
        }

        /// <summary>
        /// Deletes the line from the basket.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public ActionResult Delete(string id)
        {
            CatfishApi.DeleteOrderLineItem(id);

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Updates the quantity in the catfish basket.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="quantity">The new quantity.</param>
        /// <returns></returns>
        public ActionResult UpdateQuantity(string id, int quantity)
        {
            CatfishApi.UpdateOrderLineItemQuantity(id, quantity);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Display the checkout confirmation.
        /// </summary>
        /// <param name="newOrderId">The new order identifier.</param>
        /// <returns></returns>
        public ActionResult CheckoutConfirm(int newOrderId)
        {
            //Load order info from Catfish
            var orderInfo = CatfishApi.GetOrderItem(newOrderId);

            return View(orderInfo);
        }

        /// <summary>
        /// Migrates the cart from the anonymous user to the registered user.
        /// </summary>
        /// <param name="newUid">The User Id to migrate the card items to</param>
        /// <returns></returns>
        public ActionResult MigrateCart(Guid newUid)
        {
            var currentId = SessionVariables.CurrentUserId;
            //Perform the migration in Catfish
            CatfishApi.MigrateDataToUser(currentId, newUid);
            SessionVariables.CurrentUserId = newUid;

            return RedirectToAction("Index");
        }
    }
}