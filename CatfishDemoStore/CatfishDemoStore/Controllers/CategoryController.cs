﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CatfishDemoStore.Helper;
using CatfishDemoStore.Models.Catfish;
using CatfishDemoStore.Services;

namespace CatfishDemoStore.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            return View(Global.Categories ?? new List<Category>());
        }

        //
        // GET: /Category/Details/5

        public ActionResult Details(int id)
        {
            Category category = Global.Categories.SingleOrDefault(c => c.Id == id);

            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        //
        // GET: /Category/Add/5
        public ActionResult Add(int? id)
        {
            return View(id);
        }
        //
        // POST: /Category/Add/5
        [HttpPost]
        public ActionResult AddCategory(string name, string description, int? parentCategoryId)
        {
            CatfishApi.CreateCategory(name, description, parentCategoryId);

            return ReloadCatalog();
        }

        /// <summary>
        /// Reloads the catalog from Catfish.
        /// </summary>
        /// <returns></returns>
        public ActionResult ReloadCatalog()
        {
            Global.Categories = null;
            return RedirectToAction("Index");
        }
    }
}