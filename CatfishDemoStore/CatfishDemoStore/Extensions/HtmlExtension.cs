﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using CatfishDemoStore.Models.Catfish;

namespace CatfishDemoStore.Extensions
{
    public static class HtmlExtension
    {
        public static MvcHtmlString DisplayCategoryName(this HtmlHelper html, Category cat)
        {
            return MvcHtmlString.Create(GetCategoryName(cat));
        }

        private static string GetCategoryName(Category cat)
        {
            var name = string.Empty;
            if (cat.ParentCategory != null) {
                name += GetCategoryName(cat.ParentCategory);
                name += " &raquo; " + cat.Name;
            }
            else
                name = cat.Name;
            return name;
        }
    }
}