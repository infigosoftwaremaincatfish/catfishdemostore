﻿namespace CatfishDemoStore.ViewModel
{
    public class MessageViewModel
    {
        public string Message;
        public MessageType MessageType;
    }

    public enum MessageType
    {
        Success,
        Error,
        Info
    }
}