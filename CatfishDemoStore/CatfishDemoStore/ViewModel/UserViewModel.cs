﻿using System;
using CatfishApiSharp.Domain.User;

namespace CatfishDemoStore.ViewModel
{
    public class UserViewModel
    {
        public Guid CurrentUserId;
        public CustomerToken CustomerToken;
    }
}