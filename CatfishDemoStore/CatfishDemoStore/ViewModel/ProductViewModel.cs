﻿using System.Collections.Generic;
using CatfishDemoStore.Models.Catfish;

namespace CatfishDemoStore.ViewModel
{
    public class ProductViewModel
    {
        public int? ParentProductId;
        public List<Category> Categories;
    }
}