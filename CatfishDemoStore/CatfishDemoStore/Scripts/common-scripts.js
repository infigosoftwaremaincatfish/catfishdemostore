/*---LEFT BAR ACCORDION----*/
$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
        //  cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });

    // right slidebar
    $.slidebars();

    // set the menu active  according to the current url
    var _url = window.location.pathname;
    $("#sidebar .sidebar-menu a").each(function () {
        var $el = $(this),
            href = $el.attr("href");
        if (_url != "/" && _url == href) { //_url.indexOf(href) > -1
            $el.addClass("active");
            $el.parent().addClass("active");
            $el.parent().parent().parent().find(" > a").click();
        } else if (_url == "/" && href == "/") {
            $el.addClass("active");
        }
    });

    // focus first input on the page
    $('*:input[type!=hidden]:first').focus();

    // add feature to sidebar to be sizeable
    $('#dragbar').mousedown(function () {
        $(document).mousemove(function (e) {
            var minWidth = 250;
            var w = e.pageX;
            if (w < minWidth) w = minWidth; // min value of w is 250px
            $('#sidebar').css("width", w);
            $('#main-content').css("margin-left", w + "px");
            // save the w value to local storage so next time we will have the exact
            localStorage.setItem("sidebar-w", w);
            e = e || window.event;

            function pauseEvent(e) {
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();
                e.cancelBubble = true;
                e.returnValue = false;
                return false;
            }

            pauseEvent(e);
        });
        $(document).mouseup(function () {
            $(document).unbind('mousemove');
        });
    });
});

var Script = function () {

    //  sidebar dropdown menu auto scrolling
    jQuery('#sidebar .sub-menu > a').click(function () {
        var o = ($(this).offset());
        diff = 250 - o.top;
        if(diff>0)
            $("#sidebar").scrollTo("-="+Math.abs(diff),500);
        else
            $("#sidebar").scrollTo("+="+Math.abs(diff),500);
    });

    // sidebar toggle
    $(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#container').addClass('sidebar-close');
                $('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                $('#container').removeClass('sidebar-close');
                $('#sidebar > ul').show();

                // load sidebar details according to the saved value in local storage
                var sidebarW = localStorage.getItem("sidebar-w") ? localStorage.getItem("sidebar-w") : 250;
                $('#sidebar').css("width", sidebarW);
                $('#main-content').css("margin-left", sidebarW + "px");
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });

    $('.fa-bars').click(function () {
        var sw = 250; // default w of sidebar is 250px
        if (localStorage.getItem("sidebar-w")) {
            sw = localStorage.getItem("sidebar-w");
        }
        if ($('#sidebar > ul').is(":visible") === true) {
            $('#main-content').css({ 'margin-left': '0px' });
            $('#sidebar').css({ 'margin-left': '-'+sw+'px' });
            $('#sidebar > ul').hide();
            $("#container").addClass("sidebar-closed");
        } else {
            $('#main-content').css({ 'margin-left': sw + 'px' });
            $('#sidebar > ul').show();
            $('#sidebar').css({ 'margin-left': '0' });
            $("#container").removeClass("sidebar-closed");
        }
    });

    // custom scrollbar
    $("#sidebar").niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '3', cursorborderradius: '10px', background: '#404040', spacebarenabled:false, cursorborder: ''});

    $("html").niceScroll({ styler: "fb", cursorcolor: "#0079B1", cursorwidth: '6', cursorborderradius: '10px', background: '#404040', spacebarenabled: false, cursorborder: '', zindex: '1000' });

    // widget tools
    jQuery('.panel .tools .fa-chevron-down').click(function () {
        var el = jQuery(this).parents(".panel").children(".panel-body");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });

    jQuery('.panel .tools .fa-times').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });


    //    tool tips
    $('.tooltips').tooltip();

    //    popovers
    $('.popovers').popover();
    var substringMatcher = function(strs) {
            return function findMatches(q, cb) {
                var matches, substrRegex;

                // an array that will be populated with substring matches
                matches = [];

                // regex used to determine if a string contains the substring `q`
                substrRegex = new RegExp(q, 'i');

                // iterate through the pool of strings and for any string that
                // contains the substring `q`, add it to the `matches` array
                $.each(strs, function(i, str) {
                    if (substrRegex.test(str)) {
                        // the typeahead jQuery plugin expects suggestions to a
                        // JavaScript object, refer to typeahead docs for more info
                        matches.push({ value: str });
                    }
                });

                cb(matches);
            };
        };
}();

var substringMatcher = function (strs) {
    return function findMatches(q, cb) {
        var matches, substrRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function (i, str) {
            if (substrRegex.test(str)) {
                // the typeahead jQuery plugin expects suggestions to a
                // JavaScript object, refer to typeahead docs for more info
                matches.push({ value: str });
            }
        });

        cb(matches);
    };
};