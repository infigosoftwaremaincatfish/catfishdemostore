﻿using System.Configuration;

namespace CatfishDemoStore.Helper
{
    public static class Configuration
    {
        public static string CatfishApiBaseAddress
        {
            get
            {
                return ConfigurationManager.AppSettings["CatfishApiBaseAddress"];
            }
        }

        public static string CatfishApiKey
        {
            get
            {
                return ConfigurationManager.AppSettings["CatfishApiKey"];
            }
        }
    }
}