﻿using System.Collections.Generic;
using CatfishApiSharp.Domain.OrderClient;
using CatfishDemoStore.Models;
using CatfishDemoStore.Models.Catfish;
using CatfishDemoStore.Services;

namespace CatfishDemoStore.Helper
{
    public static class Global
    {
        private static List<Category> _categories;
        private static List<Product> _products;
        private static IEnumerable<OrderItem> _orders;

        public static List<Category> Categories
        {
            get { return _categories ?? (_categories = CatfishApi.GetCatfishCategories()); }
            set { _categories = value; }
        }

        public static List<Product> Products
        {
            get { return _products ?? (_products = CatfishApi.GetCatfishProducts()); }
            set { _products = value; }
        }

        public static IEnumerable<OrderItem> Orders
        {
            get { return _orders ?? (_orders = CatfishApi.GetOrderListForUser(SessionVariables.CurrentUserId, null)); }
            set { _orders = value; }
        }
    }
}