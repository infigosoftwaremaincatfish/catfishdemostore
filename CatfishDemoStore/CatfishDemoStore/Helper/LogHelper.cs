﻿using System;
using System.Collections.Generic;
using log4net;
using log4net.Appender;

namespace CatfishDemoStore.Helper
{
    public class LogHelper
    {
        private static bool _hasLoggedErrors;
        static readonly ILog LoggerInstance = LogManager.GetLogger("Logger");
        static readonly ILog InfoLoggerInstance = LogManager.GetLogger("InfoLogger");
        static readonly ILog ErrorLoggerInstance = LogManager.GetLogger("ErrorLogger");

        public static ILog Logger
        {
            get { return LoggerInstance; }
        }

        public static ILog InfoLogger
        {
            get { return InfoLoggerInstance; }
        }

        static ILog ErrorLogger
        {
            get { return ErrorLoggerInstance; }
        }

        public static void Error(object message)
        {
            _hasLoggedErrors = true;
            ErrorLoggerInstance.Error(message);
        }

        public static void Error(object message, Exception exception)
        {
            _hasLoggedErrors = true;
            ErrorLoggerInstance.Error(message, exception);
        }
        public static void ErrorFormat(string format, object arg1, object arg2)
        {
            _hasLoggedErrors = true;
            ErrorLoggerInstance.ErrorFormat(format, arg1, arg2);
        }

        public static String GetErrorLogFileName()
        {
            String filename = null;

            IAppender[] appenders = ErrorLoggerInstance.Logger.Repository.GetAppenders();
            // Check each appender this logger has
            foreach (IAppender appender in appenders)
            {
                Type t = appender.GetType();
                // Get the file name from the first FileAppender found and return
                if (t == typeof(FileAppender) || t == typeof(RollingFileAppender))
                {
                    filename = ((FileAppender)appender).File;
                    break;
                }
            }
            return filename;
        }

        public static void Warning(object message)
        {
            ErrorLoggerInstance.Error(message);
        }

        public static void Info(object message)
        {
            ErrorLoggerInstance.Info(message);
        }

        public static void Info(object message, Exception exception)
        {
            ErrorLoggerInstance.Info(message, exception);
        }

        public static bool HasLoggedErrors
        {
            get { return _hasLoggedErrors; }
        }

        public static void ResetHasLoggedErrors()
        {
            _hasLoggedErrors = false;
        }

        public static IEnumerable<ILog> Loggers
        {
            get
            {
                return new[]
                {
                    Logger,
                    InfoLogger,
                    ErrorLogger
                };
            }
        }
    }
}
