﻿using System;
using System.Web;

namespace CatfishDemoStore.Helper
{
    public static class SessionVariables
    {
        public static Guid CurrentUserId
        {
            get
            {
                var sessionUserId = HttpContext.Current.Session["UserId"];
                if (sessionUserId == null)
                {
                    HttpContext.Current.Session.Add("UserId", Guid.NewGuid());
                }
                return (Guid)HttpContext.Current.Session["UserId"];
            }
            set { HttpContext.Current.Session.Add("UserId", value); }
        }
    }
}