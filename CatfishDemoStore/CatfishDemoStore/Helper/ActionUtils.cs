﻿using System;

namespace CatfishDemoStore.Helper
{
    class ActionUtils
    {
        public static void RunSafe(Action action, string customMessage = null)
        {
            try
            {
                action();
            }
            catch (AggregateException ex)
            {
                LogError(customMessage, ex);
            }
            catch (Exception ex)
            {
                LogError(customMessage, ex);
            }
        }

        public static void RunSafe<T>(Action<T> action, T obj, string customMessage = null)
        {
            try
            {
                action(obj);
            }
            catch (AggregateException ex)
            {
                LogError(customMessage, ex);
            }
            catch (Exception ex)
            {
                LogError(customMessage, ex);
            }
        }

        public static T RunSafe<T>(Func<T> func, string customMessage = null)
        {
            var result = default(T);
            try
            {
                result = func();
            }
            catch (AggregateException ex)
            {
                LogError(customMessage, ex);
            }
            catch (Exception ex)
            {
                LogError(customMessage, ex);
            }
            return result;
        }

        public static TResult RunSafe<T, TResult>(Func<T, TResult> func, T obj, string customMessage = null)
        {
            var result = default(TResult);
            try
            {
                result = func(obj);
            }
            catch (AggregateException ex)
            {
                LogError(customMessage, ex);
            }
            catch (Exception ex)
            {
                LogError(customMessage, ex);
            }
            return result;
        }

        static void LogError(string customMessage, Exception ex)
        {
            if (!string.IsNullOrWhiteSpace(customMessage))
            {
                //LogHelper.ErrorFormat("{0}. {1}", customMessage, ex);
            }
            else
            {
                //LogHelper.Error(ex);
            }
        }
    }
}