﻿using System.IO;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using CatfishDemoStore.Services;
using log4net.Config;

namespace CatfishDemoStore
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            XmlConfigurator.ConfigureAndWatch(new FileInfo(@Server.MapPath("~/log4net.config")));

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
