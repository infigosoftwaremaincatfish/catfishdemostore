﻿using CatfishApiSharp.Domain.Catalog;

namespace CatfishDemoStore.Models
{
    /// <summary>
    /// Represents a product
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Gets or sets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>
        /// The price.
        /// </value>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the product art URL.
        /// </summary>
        /// <value>
        /// The product art URL.
        /// </value>
        public string ProductArtUrl { get; set; }

        public ProductInfo.ItemType Type { get; set; }
    }
}
