﻿using System.Collections.Generic;
using CatfishApiSharp.Domain.OrderClient;
using CatfishApiSharp.Domain.OrderLine;

namespace CatfishDemoStore.Models
{
    public class Order : OrderItem
    {
        public IEnumerable<OrderLineItem> Products { get; set; }

        public Order(OrderItem order)
        {
            Id = order.Id;
            BillingAddress = order.BillingAddress;
            OrderLineItems = order.OrderLineItems;
            OrderTotal = order.OrderTotal;
            PurchaseOrderNumber = order.PurchaseOrderNumber;
            ShippingAddress = order.ShippingAddress;
            ShippingCost = order.ShippingCost;
            ShippingType = order.ShippingType;
            Status = order.Status;
            Tax = order.Tax;
            UserIdentifier = order.UserIdentifier;
        }
    }
}