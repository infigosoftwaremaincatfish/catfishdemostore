﻿using System.Collections.Generic;
using CatfishApiSharp.Domain.Catalog;

namespace CatfishDemoStore.Models.Catfish
{
    public class Category : CategoryInfo
    {
        public Category()
        {
            Products = new List<Product>();
        }
        public Category ParentCategory;
        public List<Product> Products;
    }
}