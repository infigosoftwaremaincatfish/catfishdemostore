﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using CatfishApiSharp;
using CatfishApiSharp.Domain;
using CatfishApiSharp.Domain.Catalog;
using CatfishApiSharp.Domain.Common;
using CatfishApiSharp.Domain.OrderClient;
using CatfishApiSharp.Domain.OrderLine;
using CatfishApiSharp.Domain.User;
using CatfishDemoStore.Helper;
using CatfishDemoStore.Models;
using CatfishDemoStore.Models.Catfish;
using WebGrease.Css.Extensions;
using CustomerToken = CatfishApiSharp.Domain.User.CustomerToken;
using DownloadJob = CatfishApiSharp.Domain.Printing.DownloadJob;

namespace CatfishDemoStore.Services
{
    public static class CatfishApi
    {
        private static ICatfishApiClient _catfishApiClient;
        public static ICatfishApiClient CatfishApiClient
        {
            get {
                return _catfishApiClient ??
                       (_catfishApiClient =
                           new CatfishApiClient(Configuration.CatfishApiBaseAddress, false, Configuration.CatfishApiKey,
                               String.Empty));
            }
        }

        #region Categories

        public static List<Category> GetCatfishCategories()
        {
            return ActionUtils.RunSafe(() =>
            {
                var catfishCategIds = CatfishApiClient.Catalog.GetCategoryList();
                var catfishCategories = new List<Category>();
                AppendCategoryDetails(catfishCategories, null, catfishCategIds);
                return catfishCategories;
            });
        }

        static void AppendCategoryDetails(List<Category> catfishCategories, Category parent, IEnumerable<int> ids)
        {
            foreach (var id in ids)
            {
                var categoryDetails = CreateLocalCategory(CatfishApiClient.Catalog.GetCategoryInfo(id));
                categoryDetails.ParentCategory = parent;
                AppendCategoryDetails(catfishCategories, categoryDetails, categoryDetails.SubCategoryIds);
                var prodInCateg = GetCatfishProducts(categoryDetails.Id).Select(CreateLocalProduct).ToList();
                categoryDetails.Products = prodInCateg;
                catfishCategories.Add(categoryDetails);
            }
        }

        static Category CreateLocalCategory(CategoryInfo ci)
        {
            return new Category
            {
                Description = ci.Description,
                Id = ci.Id,
                Name = ci.Name,
                PreviewUrl = ci.PreviewUrl,
                ProductIds = ci.ProductIds,
                SubCategoryIds = ci.SubCategoryIds,
                ThumbnailUrl = ci.ThumbnailUrl
            };
        }

        public static void CreateCategory(string name, string description, int? parentCategoryId)
        {
            ActionUtils.RunSafe(() =>
            {
                var cpr = new CreateCategoryRequest
                {
                    Name = name,
                    ParentCategoryId = parentCategoryId,
                    Description = description
                };
                CatfishApiClient.Catalog.CreateCategory(cpr);
            });
        }

        #endregion

        #region Products

        public static List<Product> GetCatfishProducts()
        {
            return ActionUtils.RunSafe(() =>
            {
                var productsIds = CatfishApiClient.Catalog.GetProductList();
                var products = new List<Product>();
                productsIds.ForEach(pId => products.Add(CreateLocalProduct(CatfishApiClient.Catalog.GetProductInfo(pId))));
                return products;
            });
        }

        public static IEnumerable<ProductInfo> GetCatfishProducts(int id)
        {
            return ActionUtils.RunSafe(() =>
            {
                var catfishProductsIds = CatfishApiClient.Catalog.GetProductsInCategory(id);
                return catfishProductsIds.Select(pId => CatfishApiClient.Catalog.GetProductInfo(pId)).ToList();
            });
        }

        static Product CreateLocalProduct(ProductInfo pi)
        {
            return new Product
            {
                ProductId = pi.Id,
                Name = pi.Name,
                Price = pi.Price,
                ProductArtUrl = pi.ThumbnailUrls.FirstOrDefault(),
                Type = pi.Type
            };
        }

        public static ProductInfo GetProduct(int id)
        {
            return ActionUtils.RunSafe(() => CatfishApiClient.Catalog.GetProductInfo(id));
        }
        public static void UpdateProduct(int id, string name, string description, decimal? price)
        {
            ActionUtils.RunSafe(() =>
            {
                var upr = new UpdateProductRequest
                {
                    ProductId = id,
                    Name = name,
                    Description = description
                };
                if (price.HasValue)
                    upr.Price = price.Value;

                CatfishApiClient.Catalog.UpdateProduct(upr);
            });
        }
        
        public static void CreateProduct(string name, string description, decimal? price, int[] categories, int? parentProductId)
        {
            ActionUtils.RunSafe(() =>
            {
                var cpr = new CreateProductRequest
                {
                    Name = name,
                    Description = description,
                    CreateFromProductId = parentProductId
                };

                if (price.HasValue)
                    cpr.Price = price.Value;
                if (categories != null)
                    cpr.CategoryIds = new int[0];

                return CatfishApiClient.Catalog.CreateProduct(cpr);
            });
        }

        public static void UploadProductImage(int productId, string thumbPath)
        {
            ActionUtils.RunSafe(() =>
            {
                CatfishApiClient.Catalog.UploadProductImage(productId, thumbPath);
            });
        }

        public static void UploadPdf(int id, string pdfFilePath)
        {
            ActionUtils.RunSafe(() =>
            {
                CatfishApiClient.Catalog.UploadPdf(id, pdfFilePath);
            });
        }
        #endregion

        #region Orders

        public static IEnumerable<OrderItem> GetOrderListForUser(Guid userId, OrderItem.OrderStatus? orderStatus)
        {
            return ActionUtils.RunSafe(() =>
            {
                var orderIds = CatfishApiClient.Order.GetOrderListForUser(userId, orderStatus);
                var orders = new List<OrderItem>();
                orderIds.ForEach(oId => orders.Add(CatfishApiClient.Order.GetOrderInfo(oId)));
                return orders;
            });
        }

        public static OrderItem GetOrderItem(int id)
        {
            return ActionUtils.RunSafe(() => CatfishApiClient.Order.GetOrderInfo(id));
        }
        public static IEnumerable<OrderLineItem> SearchByNotes(string notes)
        {
            return ActionUtils.RunSafe(() => CatfishApiClient.Orderline.SearchByNotes(notes));
        }
        public static void Reorder(int id)
        {
            ActionUtils.RunSafe(() => CatfishApiClient.Order.Reorder(id));
        }
        public static void ChangeOrderStatus(int id, OrderItem.OrderStatus status)
        {
            ActionUtils.RunSafe(() => CatfishApiClient.Order.ChangeOrderStatus(id, status));
        }
        public static void SetPurchaseOrderNumber(int id, string purchaseOrderNumber)
        {
            ActionUtils.RunSafe(() => CatfishApiClient.Order.SetPurchaseOrderNumber(id, purchaseOrderNumber));
        }
        #endregion

        #region OrderLine

        public static IEnumerable<string> GetOrderLineIdsForUser(Guid userId)
        {
            return ActionUtils.RunSafe(() => CatfishApiClient.Orderline.GetBasketItemsForUser(userId));
        }
        public static OrderLineItem GetOrderLineDetails(string orderLineId)
        {
            return ActionUtils.RunSafe(() => CatfishApiClient.Orderline.GetOrderLineItemInfo(orderLineId));
        }
        public static int PlaceOrder(Guid userId, Address address)
        {
            return ActionUtils.RunSafe(() => CatfishApiClient.Order.PlaceOrder(userId, address, address, null, false));
        }
        public static void DeleteOrderLineItem(string orderLineId)
        {
            ActionUtils.RunSafe(() => CatfishApiClient.Orderline.DeleteOrderLineItem(orderLineId));
        }
        public static void UpdateOrderLineItemQuantity(string orderLineId, int quantity)
        {
            ActionUtils.RunSafe(() => CatfishApiClient.Orderline.UpdateOrderLineItemQuantity(orderLineId, quantity));
        }
        public static void UpdateOrderLineItemStatus(string id, int status)
        {
            ActionUtils.RunSafe(() => CatfishApiClient.Order.UpdateOrderLineItemInfo(id, (OrderLineItem.ItemStatus)status));
        }
        public static void ReorderItem(string id)
        {
            ActionUtils.RunSafe(() => CatfishApiClient.Orderline.Reorder(id));
        }
        public static void SetOrderLineItemNotes(string id, string notes)
        {
            ActionUtils.RunSafe(() => CatfishApiClient.Orderline.SetNotes(id, notes));
        }
        #endregion

        #region Users
        public static void MigrateDataToUser(Guid currentUserId, Guid newUserId)
        {
            ActionUtils.RunSafe(() => CatfishApiClient.User.Migrate(currentUserId, newUserId));
        }

        public static CustomerToken GetCustomerCookieToken(Guid userId)
        {
            return ActionUtils.RunSafe(() => CatfishApiClient.User.GetCustomerCookieToken(userId));
        }

        public static string GetCustomerSingleSignOnUrl(Guid userId, string returnUrl)
        {
            return ActionUtils.RunSafe(() =>
            {
                var result = CatfishApiClient.User.GetCustomerSingleSignOnUrl(userId, returnUrl);

                return "http://" + Configuration.CatfishApiBaseAddress + result.LoginUrl;
            });
        }

        public static void CreateCustomer(string email, string username, string password)
        {
            ActionUtils.RunSafe(() => CatfishApiClient.User.CreateUpdateCustomer(new Customer()
            {
                Email = email,
                Password = password,
                Username = username
            }));
        }
        public static Customer GetCustomerByEmail(string email)
        {
            return ActionUtils.RunSafe(() => CatfishApiClient.User.GetCustomer(email));
        }
        public static CustomerSSOResult GetCustomerSSOResultByEmail(string email, string returnUrl)
        {
            return ActionUtils.RunSafe(() => CatfishApiClient.User.GetCustomerSingleSignOnUrl(email, returnUrl));
        }
        public static CustomerToken GetCustomerTokenByEmail(string email)
        {
            return ActionUtils.RunSafe(() => CatfishApiClient.User.GetCustomerCookieToken(email));
        }
        #endregion

        #region Catfish Sync

        public static IEnumerable<HotFolder> GetAllHotFolders()
        {
            return ActionUtils.RunSafe(() => CatfishApiClient.AllHotFolders());
        }

        public static IEnumerable<HotFolder> GetHotFoldersByPrintLocationId(string id)
        {
            return ActionUtils.RunSafe(() => CatfishApiClient.GetHotFoldersByPrintLocationId(id));
        }
        
        public static IEnumerable<PrintLocation> GetAllPrintLocations()
        {
            return ActionUtils.RunSafe(() => CatfishApiClient.AllPrintLocations());
        }
        public static PrintLocation GetPrintLocationByName(string name)
        {
            return ActionUtils.RunSafe(() => CatfishApiClient.GetPrintLocationByName(name));
        }
        public static DownloadJob GetNextDownloadJob(int hotFolderId)
        {
            return ActionUtils.RunSafe(() => CatfishApiClient.Printing.GetNextDownloadJob(hotFolderId.ToString(CultureInfo.InvariantCulture)));
        }
        public static void DownloadFile(string downloadId, string fileFullPath)
        {
            ActionUtils.RunSafe(() => CatfishApiClient.Printing.DownloadFile(downloadId, fileFullPath));
        }
        public static void SetJobAsDownloaded(string downloadId)
        {
            ActionUtils.RunSafe(() => CatfishApiClient.Printing.SetJobAsDownloaded(downloadId));
        }
        public static void SetJobAsFailedToDownload(string downloadId)
        {
            ActionUtils.RunSafe(() => CatfishApiClient.Printing.SetJobAsFailedToDownload(downloadId));
        }

        public static void SetOrderAsDownloaded(int iopId)
        {
            ActionUtils.RunSafe(() => CatfishApiClient.Printing.SetOrderAsDownloaded(iopId));
        }
        public static void SetOrderAsVerified(int iopId)
        {
            ActionUtils.RunSafe(() => CatfishApiClient.Printing.SetOrderAsVerified(iopId));
        }

        public static IEnumerable<UploadLocation> GetAllUploadLocations()
        {
            return ActionUtils.RunSafe(() => CatfishApiClient.AllUploadLocations());
        }
        public static UploadLocation GetUploadLocationByName(string name)
        {
            return ActionUtils.RunSafe(() => CatfishApiClient.GetUploadLocationByName(name));
        }
        #endregion
    }
}