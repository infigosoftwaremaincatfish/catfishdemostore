﻿using System.Web.Optimization;

namespace CatfishDemoStore
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            // load scripts for navigation and sidebar
            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                      "~/Scripts/jquery.dcjqaccordion.2.7.js",
                      "~/Scripts/jquery.unobtrusive-ajax.min.js",
                      "~/Scripts/jquery.scrollTo.min.js",
                      "~/Scripts/jquery.nicescroll.js",
                      "~/Scripts/slidebars.min.js",
                      "~/Scripts/common-scripts.js",
                      "~/Scripts/typeahead.bundle.js"));
            bundles.Add(new ScriptBundle("~/bundles/syntax-highlighting").Include(
                      "~/Scripts/jquery.snippet.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-reset.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/site.css",
                      "~/Content/style-responsive.css"));

            bundles.Add(new StyleBundle("~/Content/syntax-highlighting").Include(
                      "~/Content/jquery.snippet.min.css"));
        }
    }
}
