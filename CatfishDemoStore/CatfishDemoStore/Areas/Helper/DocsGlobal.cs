﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using CatfishDemoStore.Areas.Docs.Models;
using File = CatfishDemoStore.Areas.Docs.Models.File;

namespace CatfishDemoStore.Areas.Helper
{
    public static class DocsGlobal
    {
        private static List<Folder> _folders;
        private static List<FileInfo> _files;

        public static List<Folder> Folders
        {
            get
            {
                if (_folders == null)
                {
                    var docsRootFolder = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/Content/Documentation"));
                    _folders =new List<Folder>();
                    foreach (var di in docsRootFolder.EnumerateDirectories())
                    {
                        _folders.Add(new Folder
                        {
                            FolderName = di.Name,
                            Files = 
                                di.EnumerateFileSystemInfos()
                                .Select(fi => new File
                                {
                                    FullName = fi.FullName,
                                    IsMethod = !fi.Name.StartsWith("_"),
                                    Name = fi.Name.Replace(".html", string.Empty).Replace("_", string.Empty).Trim()
                                }).OrderByDescending(f => !f.IsMethod).ToList()
                        });
                    }
                }
                return _folders;
            }
            set { _folders = value; }
        }

        public static List<FileInfo> Files
        {
            get
            {
                if (_files == null)
                {
                    var docsRootFolder = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/Content/Documentation"));
                    _files = docsRootFolder.EnumerateFiles("*", SearchOption.TopDirectoryOnly).ToList();
                }
                return _files;
            }
            set { _files = value; }
        }
    }
}