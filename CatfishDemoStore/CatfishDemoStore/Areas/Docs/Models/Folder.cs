﻿using System.Collections.Generic;

namespace CatfishDemoStore.Areas.Docs.Models
{
    public class Folder
    {
        public string FolderName { get; set; }
        public List<File> Files { get; set; }
        public List<Folder> ChildFolders { get; set; }
    }
}