﻿using System;
using System.Globalization;
using System.Linq;

namespace CatfishDemoStore.Areas.Docs.Models
{
    public class File
    {
        public string FullName { get; set; }
        public string Name { get; set; }
        public bool IsMethod { get; set; }

        public string ToString()
        {
            return
                !IsMethod
                    ? Name
                    : string.Concat(Name.Select(x => Char.IsUpper(x) ? " " + x : x.ToString(CultureInfo.InvariantCulture)))
                        .TrimStart(' ');
        }
    }
}