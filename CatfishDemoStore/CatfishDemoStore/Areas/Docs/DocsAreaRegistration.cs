﻿using System.Security.Policy;
using System.Web.Mvc;

namespace CatfishDemoStore.Areas.Docs
{
    public class DocsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Docs";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Docs_default",
                url: "Docs/{folder}/{file}",
                defaults: new {area = "Docs", controller = "Home", action = "Index", folder = UrlParameter.Optional, file = UrlParameter.Optional},
                namespaces: new[] {"CatfishDemoStore.Areas.Docs.Controllers"} // Namespace of controllers in root area
                );
        }
    }
}