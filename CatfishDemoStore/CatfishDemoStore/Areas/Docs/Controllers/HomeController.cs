﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CatfishDemoStore.Areas.Helper;

namespace CatfishDemoStore.Areas.Docs.Controllers
{
    public class HomeController : Controller
    {
        // GET: Docs/Home
        public ActionResult Index(string folder, string file)
        {
            DocsGlobal.Folders = null;
            if (!string.IsNullOrEmpty(folder) && !string.IsNullOrEmpty(file))
            {
                var foundFolder = DocsGlobal.Folders.SingleOrDefault(fold => fold.FolderName == folder);
                if (foundFolder != null)
                {
                    var foundFile = foundFolder.Files.FirstOrDefault(sf => sf.Name == file);
                    if (foundFile != null)
                    {
                        ViewBag.Content = System.IO.File.ReadAllText(foundFile.FullName);
                    }
                }
            }
            // load file from parent directory
            else if (!string.IsNullOrEmpty(folder) && string.IsNullOrEmpty(file))
            {
                var foundFile = DocsGlobal.Files.FirstOrDefault(f => f.Name == folder);
                if (foundFile != null)
                {
                    ViewBag.Content = System.IO.File.ReadAllText(foundFile.FullName);
                }
            }

            return View();
        }
    }
}